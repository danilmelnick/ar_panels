﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ARButton : MonoBehaviour
{

	public UnityEvent clickEvent;


	void OnMouseDown ()
    {
		if (!EventSystem.current.IsPointerOverGameObject ()) {
			Click ();
		}
    }

	void OnPointerDown ()
	{
		if (!EventSystem.current.IsPointerOverGameObject ()) {
			Click ();
		}
	}

	public void Click ()
	{
		if (clickEvent != null) {
			clickEvent.Invoke ();
			 
		}
	}
}
