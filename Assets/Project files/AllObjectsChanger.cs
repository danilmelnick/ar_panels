﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


 


public class AllObjectsChanger : MonoBehaviour {

	
    public void ChangeAllObjectsToRound()
    {
        foreach (ObjectsChanger oc in gameObject.GetComponentsInChildren<ObjectsChanger>())
        {
            oc.SetRoundPref();
        }
    }


    public void ChangeAllObjectsToStripes()
    {
        foreach (ObjectsChanger oc in gameObject.GetComponentsInChildren<ObjectsChanger>())
        {
            oc.SetStripesPref();
        }
    }

}
