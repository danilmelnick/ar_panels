﻿using UnityEngine;
using UnityEngine.Events;

public class DelayedEvent : MonoBehaviour {

	public float delay = 0.5f;
	public UnityEvent _event;

	void OnEnable () 
	{
		Invoke ("InvokeEvent", delay);
	}



     public void InvokeEvent ()
	{
            _event.Invoke ();
	}
}

