﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkSizesToScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    [Range (0, 1)]
    public float widePercent;
    [Range(0, 1)]
    public float heightPercent;

    void Update () {
        GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.currentResolution.width * widePercent/2f, Screen.currentResolution.height * heightPercent/2f);
	}
}
