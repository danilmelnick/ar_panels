﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{

    public bool MoveX_p;
    public bool MoveX_m;
    public bool MoveZ_p;
    public bool MoveZ_m;

    public float moveSpeed = 0.1f;

    public void EnableMoveX_p()
    {
        MoveX_p = true;
    }

    public void EnableMoveX_m()
    {
        MoveX_m = true;
    }

    public void EnableMoveZ_p()
    {
        MoveZ_p = true;
    }

    public void EnableMoveZ_m()
    {
        MoveZ_m = true;
    }





    public void DisableMoveX_p()
    {
        MoveX_p = false;
    }

    public void DisableMoveX_m()
    {
        MoveX_m = false;
    }

    public void DisableMoveZ_p()
    {
        MoveZ_p = false;
    }

    public void DisableMoveZ_m()
    {
        MoveZ_m = false;
    }





    // Update is called once per frame
    void Update()
    {
        if (MoveX_p)
        {
            gameObject.transform.Translate(new Vector3(moveSpeed, 0, 0));
        }

        if (MoveX_m)
        {
            gameObject.transform.Translate(new Vector3(-moveSpeed, 0, 0));
        }


        if (MoveZ_p)
        {
            gameObject.transform.Translate(new Vector3(0, 0, moveSpeed));
        }


        if (MoveZ_m)
        {
            gameObject.transform.Translate(new Vector3(0,0, -moveSpeed));
        }


 
    }
}
