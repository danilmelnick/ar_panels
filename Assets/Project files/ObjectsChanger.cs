﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsChanger : MonoBehaviour
{

    public GameObject[] partsOfRoundPrefab;
    public GameObject[] partsOfStripesPrefab;



    public void SetRoundPref()
    {
        foreach (GameObject pr in partsOfRoundPrefab)
        {
            pr.SetActive(true);
        }
        foreach (GameObject ps in partsOfStripesPrefab)
        {
            ps.SetActive(false);
        }
    }

    public void SetStripesPref()
    {
        foreach (GameObject pr in partsOfRoundPrefab)
        {
            pr.SetActive(false);
        }
        foreach (GameObject ps in partsOfStripesPrefab)
        {
            ps.SetActive(true);
        }
    }
}
