﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelMultiplier : MonoBehaviour {

    public GameObject panelInstance;
    public float offsetX;
    public float offsetY;

    public InputField leftAddition;
    public InputField rightAddition;
    public InputField upAddition;

    public List<GameObject> horizontalPanels;
    public List<GameObject> allInstanciatedPanels;
    public void PanelsCountToZero()
    {
        FindObjectOfType<GoogleARCore.Examples.AR_PANELS.AR_Panels_Controller>().SetCountToZero();
    }
    void Start()
    {
        DeleteAllInstanciatedPanels();
        RebuilAllPanels();
    }
    public void DeleteAllInstanciatedPanels()
    {
        foreach (GameObject g in allInstanciatedPanels)
        {
            GameObject.Destroy(g, 0.001f);
        }
        allInstanciatedPanels.Clear();
        horizontalPanels.Clear();
    }

    public void SetInputsToZero()
    {
        leftAddition.text = "0";
        rightAddition.text = "0";
        upAddition.text = "0";
    }

    public void RebuilAllPanels()
    {
        DeleteAllInstanciatedPanels();
        horizontalPanels = new List<GameObject>();
        allInstanciatedPanels= new List<GameObject>();

        for (int i=0; i< int.Parse(leftAddition.text);i++)
        {
            GameObject newpanel = GameObject.Instantiate(panelInstance, panelInstance.transform.parent);
            newpanel.transform.Translate(Vector3.left*offsetX*(i+1), Space.Self);
            horizontalPanels.Add(newpanel);
            allInstanciatedPanels.Add(newpanel);
        }


        for (int i = 0; i < int.Parse(rightAddition.text); i++)
        {
            GameObject newpanel = GameObject.Instantiate(panelInstance, panelInstance.transform.parent);
            newpanel.transform.Translate(Vector3.right * offsetX * (i + 1), Space.Self);
            horizontalPanels.Add(newpanel);
            allInstanciatedPanels.Add(newpanel);
        }

        for (int i = 0; i < int.Parse(upAddition.text); i++)
        { 

            foreach (GameObject hp in horizontalPanels)
            {
                GameObject newpanel = GameObject.Instantiate(hp, hp.transform.parent);
                newpanel.transform.Translate(Vector3.up * offsetY * (i + 1), Space.Self);
                allInstanciatedPanels.Add(newpanel);
            }

            GameObject newpanelOfOriginal = GameObject.Instantiate(panelInstance, panelInstance.transform.parent);
            newpanelOfOriginal.transform.Translate(Vector3.up * offsetY * (i + 1), Space.Self);
            allInstanciatedPanels.Add(newpanelOfOriginal);
        }
    }

    
}
