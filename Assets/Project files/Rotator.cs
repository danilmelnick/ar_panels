﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    public bool LeftRot;
    public bool RightRot;
     


    public void EnableLeftRotation()
    {
        LeftRot = true;
    }

    public void EnableRightRotation()
    {
        RightRot = true;
    }

    public void DisableLeftRotation()
    {
        LeftRot = false;
    }

    public void DisbleRightRotation()
    {
        RightRot = false;
    }
    // Update is called once per frame
    void Update () {
        if (LeftRot)
        {
            gameObject.transform.Rotate(new Vector3(0, 1, 0));
        }


        if (RightRot)
        {
            gameObject.transform.Rotate(new Vector3(0, -1, 0));
        }
    }
}
