﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallScreenSizeCorrect : MonoBehaviour {

    public CanvasScaler canvasScaler;
	// Use this for initialization
	void Start () {

        //1334*750  screen size of 6s
        if ((Screen.width<740)&& (Screen.height < 1324))
        {
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
            canvasScaler.scaleFactor = 1.1f;
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
