﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UIToTextFitter : MonoBehaviour {

    public TextMeshProUGUI textUI;
    public Text textUIsimple;

    public enum UI_ENUM { simple, textmeshpro};
    public UI_ENUM uI_ENUM;
    public int bottomPad=50;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        switch (uI_ENUM)
        {
            case UI_ENUM.textmeshpro:
                this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().sizeDelta.x, textUI.preferredHeight + bottomPad);
                break;
            case UI_ENUM.simple:
                this.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().sizeDelta.x, textUIsimple.preferredHeight+ bottomPad);
                break;
        }
        
	}
}
